# Repozitorij

- Namen repozitorija je hranjenje zadnje verzije diplomskega dela (.pdf) in beleženje sprememb.
- Za lažje dodajanje opomb se lahko uporabi [issue tracker](https://bitbucket.org/tomicdev/diplomska-naloga/issues?status=new&status=open).
